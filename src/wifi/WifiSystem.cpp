#include "WifiSystem.hpp"

void WifiSystem::init(void)
{
    WiFi.mode(WIFI_STA);  /* Only station, not access point */
    /* check configuration structure exists not connect automatically */
    if((int) conf.fields.wifi_ssid[0] != 255 && strlen(conf.fields.wifi_ssid) > 0) {
        String ssid = String(conf.fields.wifi_ssid);
        // Serial.print("Connecting to Wifi SSID " + String(conf.fields.wifi_ssid) + " ");
        connect(ssid, String(conf.fields.wifi_password));
        int start = millis();
        while(WifiSystem::is_connected() == false && millis() < start + WIFI_CONNECT_TIMEOUT) {
            // Serial.print('.');
            delay(1000);
        }
        // Serial.println('.');
        if(WifiSystem::is_connected() == true) {
            Serial.println("Wifi device connected to " + WifiSystem::get_SSID() + " with IP " + WifiSystem::get_IP());
        } else {
            Serial.println("Impossible to connect to " + ssid);
        }
    }
}

void WifiSystem::connect(String ssid, String password)
{
    if(is_connected() == true) {
        WiFi.disconnect();
    }
    WiFi.begin(ssid, password);
}

void WifiSystem::disconnect(void)
{
    if(is_connected() == true) {
        WiFi.disconnect();
    }
}

void WifiSystem::power_off(void)
{
    disconnect();
    WiFi.forceSleepBegin();
}
    
void WifiSystem::power_on(void)
{
    WiFi.forceSleepWake();
    delay(1);   /* it must be executed to enter in wakeup mode of the WiFi */
    init();
}
