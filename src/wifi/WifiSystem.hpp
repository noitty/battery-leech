#ifndef __WIFI_SYSTEM_HPP__
#define __WIFI_SYSTEM_HPP__

#include <Commander.h>
#include <ESP8266WiFi.h>
#include "../configuration/Configuration.hpp"

#define WIFI_CONNECT_TIMEOUT    15000    /* ms */

extern Configuration conf;

class WifiSystem
{
public:
    static void init(void);
    static void connect(String ssid, String password);
    static void disconnect(void);
    static bool is_connected(void) { return (WiFi.status() ==  WL_CONNECTED); }
    static String get_IP(void) { return WiFi.localIP().toString(); }
    static String get_SSID(void) { return WiFi.SSID(); }
    static void power_off(void);
    static void power_on(void);
};

#endif  /* __WIFI_SYSTEM_HPP__ */
