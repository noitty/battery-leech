#ifndef __STORAGE_SYSTEM_HPP__
#define __STORAGE_SYSTEM_HPP__

#include <EEPROM.h>
#include <Arduino.h>
#include "../configuration/Configuration.hpp"

extern Configuration conf;

class StorageSystem
{
public:
    static void write_conf(Configuration conf);
    static void read_conf(Configuration &conf);
    static void clear(void);
};

#endif  /* __STORAGE_SYSTEM_HPP__ */