#include "StorageSystem.hpp"

void StorageSystem::write_conf(Configuration conf)
{
    EEPROM.begin(CONF_LENGTH);
    /* write all the conf has a raw pointer */
    EEPROM.put(0, conf.fields);
    if(EEPROM.end() == false) {
        Serial.println("Error while writing");
    } else {
        Serial.println("Configuration set correctly");
    }
}

void StorageSystem::read_conf(Configuration &conf)
{
    EEPROM.begin(CONF_LENGTH);
    /* write all the conf has a raw pointer */
    EEPROM.get(0, conf.fields);
    EEPROM.end();
}

void StorageSystem::clear(void)
{
    EEPROM.begin(CONF_LENGTH);
    for(int index = 0; index < EEPROM.length(); index ++) {
        EEPROM.write(index, 0);    /* write each byte to 0 */
    }
    EEPROM.end();
}
