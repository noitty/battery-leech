#include "CommandSystem.hpp"

bool CommandSystem::handler_read(Commander &cmdr)
{
    bool correct = false;
    String type = "";
    float fvalue;
    if(cmdr.countItems() > 0) {
        while(cmdr.getString(type)) {
            if(type == "temp") {
                fvalue = sensors->read_temperature(&correct) + conf.fields.calib_temp;
                if(correct == true) {
                    cmdr.print("Temperature value: ");
                    cmdr.println(fvalue);
                } else {
                    cmdr.println("Error while reading the Temperature");
                }
            } else if(type == "hum") {
                fvalue = sensors->read_humidity(&correct) + conf.fields.calib_hum;
                if(correct == true) {
                    cmdr.print("Humidity value: ");
                    cmdr.println(fvalue);
                } else {
                    cmdr.println("Error while reading the Humidity");
                }
            } else if(type == "volt") {
                fvalue = sensors->read_voltage(&correct) * conf.fields.calib_volt;
                // fvalue = sensors->read_voltage(&correct) * 5.7;  /* Vin (15V) = Vout (3.3V) * (R1 + R2) / R1 */
                if(correct == true) {
                    cmdr.print("Battery voltage value: ");
                    cmdr.println(fvalue);
                } else {
                    cmdr.println("Error while reading the Humidity");
                }
            } else {
                cmdr.println("Unknown type of measurement; usage: measure [temp|hum|volt]");
            }
        }
    } else {
        /* measure all */
    }
    return correct;
}

bool CommandSystem::handler_conf(Commander &cmdr)
{
    cmdr.println("tap 'help' to print the available configuration commands");
    if(cmdr.transferTo(conf_commands, sizeof(conf_commands), "conf> ")) {
        cmdr.transferBack(commands, sizeof(commands), "AQ$");
    }
    return true;
}

bool CommandSystem::handler_conf_clear(Commander &cmdr)
{
    StorageSystem::clear();
    memset(conf.raw, 0, CONF_LENGTH);
    return true;
}

bool CommandSystem::handler_conf_wifi(Commander &cmdr)
{
    cmdr.println("tap 'help' to print the available configuration wifi commands");
    if(cmdr.transferTo(conf_wifi_commands, sizeof(conf_wifi_commands), "conf wifi> ")) {
        cmdr.transferBack(conf_commands, sizeof(conf_commands), "conf> ");
    }
    return true;
}

bool CommandSystem::handler_conf_wifi_get(Commander &cmdr)
{
    StorageSystem::read_conf(conf);
    cmdr.println("Configuration of the device:");
    cmdr.println("\t - Wifi SSID: " + String(conf.fields.wifi_ssid));
    cmdr.println("\t - Wifi password: " + String(conf.fields.wifi_password));
    return true;
}

bool CommandSystem::handler_conf_wifi_set(Commander &cmdr)
{
    bool correct = false;
    String type = "";
    float fvalue;
    if(cmdr.countItems() > 0) {
        while(cmdr.getString(type) == true) {
            if(type == "password") {
                cmdr.getString(type);
                type += '\0';   /* set the \0 to know when it is finished */
                memcpy(conf.fields.wifi_password, type.c_str(), type.length());
            } else if(type == "ssid") {
                cmdr.getString(type);
                type += '\0';   /* set the \0 to know when it is finished */
                memcpy(conf.fields.wifi_ssid, type.c_str(), type.length());
            } else {
                cmdr.println("Unknown type of parameter; usage: set wifi ssid #SSID password #PASSWORD");
            }
        }
        StorageSystem::write_conf(conf);
    } else {
        cmdr.println("Nothing to set if no parameter is included; usage: set [wifi|server]");
    }
    return correct;
}

bool CommandSystem::handler_conf_wifi_clear(Commander &cmdr)
{
    memset(conf.fields.wifi_ssid, 0, ITEM_SIZE);
    memset(conf.fields.wifi_password, 0, ITEM_SIZE);
    StorageSystem::write_conf(conf);
    return true;
}

bool CommandSystem::handler_conf_server(Commander &cmdr)
{
    cmdr.println("tap 'help' to print the available server configuration commands");
    if(cmdr.transferTo(conf_server_commands, sizeof(conf_server_commands), "conf server> ")) {
        cmdr.transferBack(conf_commands, sizeof(conf_commands), "conf> ");
    }
    return true;
}

bool CommandSystem::handler_conf_server_get(Commander &cmdr)
{
    StorageSystem::read_conf(conf);
    cmdr.println("Configuration of the device:");
    cmdr.println("\t - Server URL: " + String(conf.fields.influxdb_url));
    cmdr.println("\t - Server token: " + String(conf.fields.influxdb_token));
    cmdr.println("\t - Server ORG: " + String(conf.fields.influxdb_org));
    cmdr.println("\t - Server bucket: " + String(conf.fields.influxdb_bucket));
    return true;
}

bool CommandSystem::handler_conf_server_set(Commander &cmdr)
{
    bool correct = false;
    String type = "";
    float fvalue;
    if(cmdr.countItems() > 0) {
        while(cmdr.getString(type) == true) {
            if(type == "url") {
                type = cmdr.getPayloadString();
                type.trim();
                int to = type.indexOf(" ");
                if(to != -1) {
                    type = type.substring(0, to);
                }
                type += '\0';
                memcpy(conf.fields.influxdb_url, type.c_str(), type.length());
            } else if(type == "token") {
                type = cmdr.getPayloadString();
                type.trim();
                int to = type.indexOf(" ");
                if(to != -1) {
                    type = type.substring(0, to);
                }
                type += '\0';   /* set the \0 to know when it is finished */
                memcpy(conf.fields.influxdb_token, type.c_str(), type.length());
            } else if(type == "org") {
                cmdr.getString(type);
                type += '\0';   /* set the \0 to know when it is finished */
                memcpy(conf.fields.influxdb_org, type.c_str(), type.length());
            } else if(type == "bucket") {
                cmdr.getString(type);
                type += '\0';   /* set the \0 to know when it is finished */
                memcpy(conf.fields.influxdb_bucket, type.c_str(), type.length());
            } else {
                cmdr.println("Unknown type of parameter; usage: set token #TOKEN url #URL org #ORG bucket #BUCKET");
            }
        }
        StorageSystem::write_conf(conf);
    } else {
        cmdr.println("Nothing to set if no parameter is included; usage: set [wifi|server]");
    }
    return correct;
}

bool CommandSystem::handler_conf_server_clear(Commander &cmdr)
{
    memset(conf.fields.influxdb_url, 0, ITEM_SIZE);
    memset(conf.fields.influxdb_org, 0, ITEM_SIZE);
    memset(conf.fields.influxdb_token, 0, TOKEN_SIZE);
    memset(conf.fields.influxdb_bucket, 0, ITEM_SIZE);
    StorageSystem::write_conf(conf);
    return true;
}

bool CommandSystem::handler_conf_sensor(Commander &cmdr)
{
    cmdr.println("tap 'help' to print the available sensor configuration commands");
    if(cmdr.transferTo(conf_sensor_commands, sizeof(conf_sensor_commands), "conf sensor> ")) {
        cmdr.transferBack(conf_commands, sizeof(conf_commands), "conf> ");
    }
    return true;
}

bool CommandSystem::handler_conf_sensor_get(Commander &cmdr)
{
    StorageSystem::read_conf(conf);
    cmdr.println("Configuration of the device:");
    cmdr.println("\t - Measurement period: " + String(conf.fields.measure_period));
    cmdr.println("\t - Sensor ID: " + String(conf.fields.id));
    cmdr.println("\t - Calibration Temperature: " + String(conf.fields.calib_temp));
    cmdr.println("\t - Calibration Humidity: " + String(conf.fields.calib_hum));
    cmdr.println("\t - Calibration Voltage: " + String(conf.fields.calib_volt));
    cmdr.println("\t - Sleep mode enabled: " + String(conf.fields.sleep_mode));
    return true;
}

bool CommandSystem::handler_conf_sensor_set(Commander &cmdr)
{
    String type = "";
    float fvalue;
    int value;
    if(cmdr.countItems() > 0) {
        while(cmdr.getString(type) == true) {
            if(type == "period") {
                cmdr.getInt(value);
                conf.fields.measure_period = (uint32_t) value;
            } else if(type == "id") {
                cmdr.getString(type);
                type += '\0';
                memcpy(conf.fields.id, type.c_str(), type.length());
            }  else if(type == "sleep_mode") {
                cmdr.getInt(value);
                conf.fields.sleep_mode = (uint8_t) value;
            } else if(type == "calibration") {
                cmdr.getString(type);
                cmdr.getFloat(fvalue);
                if(type == "temp" || type == "temperature") {
                    conf.fields.calib_temp = fvalue;
                } else if(type == "hum" || type == "humidity") {
                    conf.fields.calib_hum = fvalue;
                } else if(type == "volt") {
                    conf.fields.calib_volt = fvalue;
                }
            } else {
                cmdr.println("Unknown type of parameter; usage: set period #PERIOD");
            }
        }
        StorageSystem::write_conf(conf);
    } else {
        cmdr.println("Nothing to set if no parameters are included; usage: set period #PERIOD");
    }
    return true;
}

bool CommandSystem::handler_conf_sensor_clear(Commander &cmdr)
{
    memset(&conf.fields.measure_period, 0, sizeof(conf.fields.measure_period));
    memset(&conf.fields.id, 0, ITEM_SIZE);
    StorageSystem::write_conf(conf);
    return true;
}

bool CommandSystem::handler_exit_conf_submodule(Commander &cmdr)
{
    cmdr.transferBack(conf_commands, sizeof(conf_commands), "conf> ");
    return true;
}

bool CommandSystem::handler_status(Commander &cmdr)
{
    cmdr.println("The status of the system:");
    cmdr.println("\t - TH sensor: " + String(sensors->is_th_init()));
    cmdr.println("\t - WiFi connection: " + String(WifiSystem::is_connected()));
    cmdr.println("\t - Database server connection: " + String(client.is_connected()));
    return true;
}

bool CommandSystem::handler_reboot(Commander &cmdr)
{ 
    cmdr.println("Rebooting the system ...");
    reboot(); 
}

bool CommandSystem::handler_wifi(Commander &cmdr)
{
    cmdr.println("tap 'help' to print the available wifi commands");
    if(cmdr.transferTo(wifi_commands, sizeof(wifi_commands), "wifi> ")) {
        cmdr.transferBack(commands, sizeof(commands), "AQ$");
    }
    return 0;
}
    
bool CommandSystem::handler_wifi_connect(Commander &cmdr)
{
    String type = "";
    String ssid = "";
    String password = "";
    unsigned long start;
    bool store = false;
    if(cmdr.countItems() >= 2) {
        while(cmdr.getString(type)) {
            if(type == "password") {
                cmdr.getString(password);
            } else if(type == "ssid") {
                cmdr.getString(ssid);
            } else if(type == "--default") {
                store = true;
            } else {
                cmdr.println("Unknown argument; usage: wifi connect ssid #SSID password #PASSWORD");
            }
        }
        cmdr.print("Connecting to Wifi " + ssid + " ");
        WifiSystem::disconnect();
        start = millis();
        WifiSystem::connect(ssid, password);
        while(WifiSystem::is_connected() == false && millis() < start + WIFI_CONNECT_TIMEOUT) {
            cmdr.print('.');
            delay(1000);
        }
        cmdr.println('.');
        if(WifiSystem::is_connected() == true) {
            cmdr.println("Wifi device connected to " + ssid + " with IP " + WifiSystem::get_IP());
            if(store == true) {
                ssid += '\0';
                memcpy(conf.fields.wifi_ssid, ssid.c_str(), ssid.length());
                password += '\0';
                memcpy(conf.fields.wifi_password, password.c_str(), password.length());
                StorageSystem::write_conf(conf);
            }
        } else {
            cmdr.println("Impossible to connect to " + ssid);
        }
    } else {
        cmdr.println("Not enough argument; usage: wifi connect ssid #SSID password #PASSWORD");
    }
    return true;
}

bool CommandSystem::handler_wifi_reconnect(Commander &cmdr)
{
    WifiSystem::disconnect();
    unsigned long start = millis();
    WifiSystem::connect(conf.fields.wifi_ssid, conf.fields.wifi_password);
    while(WifiSystem::is_connected() == false && millis() < start + WIFI_CONNECT_TIMEOUT) {
        cmdr.print('.');
        delay(1000);
    }
    cmdr.println('.');
    if(WifiSystem::is_connected() == true) {
        cmdr.println("Wifi device connected to " + WifiSystem::get_SSID() + " with IP " + WifiSystem::get_IP());
    } else {
        cmdr.println("Impossible to connect to " + String(conf.fields.wifi_ssid));
    }
    return true;
}


bool CommandSystem::handler_wifi_disconnect(Commander &cmdr)
{
    if(WifiSystem::is_connected() == true) {
        cmdr.println("Diconnecting Wifi ...");
        WifiSystem::disconnect();
        cmdr.println("Wifi device disconnected!");
    } else {
        cmdr.println("The device is not connected to any WiFi Access Point");
    }
    return true;
}

bool CommandSystem::handler_wifi_status(Commander &cmdr)
{
    if(WifiSystem::is_connected() == true) {
        cmdr.print("The device is connected to " + WifiSystem::get_SSID());
        cmdr.print(" with IP address ");
        cmdr.println(WifiSystem::get_IP());
    } else {
        cmdr.println("The device is not connected to any WiFi Access Point");
    }
    return true;
}

bool CommandSystem::handler_server(Commander &cmdr)
{
    cmdr.println("tap 'help' to print the available server commands");
    if(cmdr.transferTo(server_commands, sizeof(server_commands), "server> ")) {
        cmdr.transferBack(commands, sizeof(commands), "AQ$");
    }
    return 0;
}

bool CommandSystem::handler_server_reconnect(Commander &cmdr)
{
    client.connect();
    return true;
}

bool CommandSystem::handler_server_status(Commander &cmdr)
{
    if(client.is_connected() == true) {
        cmdr.println("The client is connected to server " + String(conf.fields.influxdb_url));
    } else {
        cmdr.println("The client is not connected to server " + String(conf.fields.influxdb_url));
    }
    return true;
}

bool CommandSystem::handler_exit_to_main(Commander &cmdr)
{
    cmdr.transferBack(commands, sizeof(commands), "AQ$");
    return true;
}

void CommandSystem::begin(Stream* serial)
{   
    m_cmd.begin(serial, commands, sizeof(commands));
    m_cmd.commanderName = "AQ$";
    m_cmd.promptChar(' ');
    m_cmd.commandPrompt(ON);
    m_cmd.echo(true);
    m_cmd.println("Type 'help' to get help");
    m_cmd.printCommandPrompt();
}

void CommandSystem::run()
{
    m_cmd.update();
}
