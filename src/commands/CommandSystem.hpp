#ifndef __COMMAND_SYSTEM_HPP__
#define __COMMAND_SYSTEM_HPP__

#include <Commander.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "../sensors/SensorSystem.hpp"
#include "../storage/StorageSystem.hpp"
#include "../configuration/Configuration.hpp"
#include "../wifi/WifiSystem.hpp"
#include "../client/DatabaseClient.hpp"

extern Configuration conf;
extern SensorSystem* sensors;
extern DatabaseClient client;

static void(* reboot) (void) = 0;

class CommandSystem
{
public:
    void begin(Stream* serial);
    void run();

    static bool handler_read(Commander &cmdr);
    static bool handler_status(Commander &cmdr);
    static bool handler_reboot(Commander &cmdr);
    
    static bool handler_conf(Commander &cmdr);
    static bool handler_conf_clear(Commander &cmdr);
    static bool handler_conf_wifi(Commander &cmdr);
    static bool handler_conf_wifi_set(Commander &cmdr);
    static bool handler_conf_wifi_get(Commander &cmdr);
    static bool handler_conf_wifi_clear(Commander &cmdr);
    static bool handler_conf_server(Commander &cmdr);
    static bool handler_conf_server_set(Commander &cmdr);
    static bool handler_conf_server_get(Commander &cmdr);
    static bool handler_conf_server_clear(Commander &cmdr);
    static bool handler_conf_sensor(Commander &cmdr);
    static bool handler_conf_sensor_set(Commander &cmdr);
    static bool handler_conf_sensor_get(Commander &cmdr);
    static bool handler_conf_sensor_clear(Commander &cmdr);
    static bool handler_exit_conf_submodule(Commander &cmdr);

    static bool handler_wifi(Commander &cmdr);
    static bool handler_wifi_connect(Commander &cmdr);
    static bool handler_wifi_disconnect(Commander &cmdr);
    static bool handler_wifi_status(Commander &cmdr);
    static bool handler_wifi_reconnect(Commander &cmdr);

    static bool handler_server(Commander &cmdr);
    static bool handler_server_status(Commander &cmdr);
    static bool handler_server_reconnect(Commander &cmdr);

    static bool handler_exit_to_main(Commander &cmdr);

private:
    Commander m_cmd;
};

const commandList_t commands[] = {
    {"read",        CommandSystem::handler_read,        "measure sensors; usage: measure [temp|hum|pow]"},
    {"status",      CommandSystem::handler_status,      "get status of the device"},
    {"conf",         CommandSystem::handler_conf,       "manage configuration parameters"},
    {"wifi",        CommandSystem::handler_wifi,        "interact with the wifi system"},
    {"server",        CommandSystem::handler_server,    "interact with the server system"},
    {"reboot",      CommandSystem::handler_reboot,      "reboot the system"},
};

const commandList_t wifi_commands[] = {
    {"connect",     CommandSystem::handler_wifi_connect,     "connect to a wifi ssid; usage: wifi connect ssid #SSID password #PASSWORD [--default]; --default option stores the ssid and password values"},
    {"disconnect",  CommandSystem::handler_wifi_disconnect,  "disconnect the device to a connected wifi ssid"},
    {"status",      CommandSystem::handler_wifi_status,      "print the status connection of the wifi device"},
    {"reconnect",   CommandSystem::handler_wifi_reconnect,   "try to connect again with the stored configuration"},
    {"exit",        CommandSystem::handler_exit_to_main,     "exit the wifi menu"},
};

const commandList_t server_commands[] = {
    {"status",      CommandSystem::handler_server_status,      "print the status connection of the remote server"},
    {"reconnect",   CommandSystem::handler_server_reconnect,   "try to connect again with the stored configuration"},
    {"exit",        CommandSystem::handler_exit_to_main,       "exit the wifi menu"},
};

const commandList_t conf_commands[] = {
    {"wifi",        CommandSystem::handler_conf_wifi,           "enter wifi configuration menu"},
    {"server",      CommandSystem::handler_conf_server,         "enter server configuration menu"},
    {"sensor",      CommandSystem::handler_conf_sensor,         "enter sensor configuration menu"},
    {"clear",       CommandSystem::handler_conf_clear,          "clear the persistent configuration"},
    {"exit",        CommandSystem::handler_exit_to_main,        "exit the configuration menu"},
};

const commandList_t conf_wifi_commands[] = {
    {"get",        CommandSystem::handler_conf_wifi_get,        "print wifi configuration;"},
    {"set",        CommandSystem::handler_conf_wifi_set,        "set wifi configuration; usage: set ssid #SSID password #PASSWORD"},
    {"clear",      CommandSystem::handler_conf_wifi_clear,      "clear the stored wifi configuration"},
    {"exit",       CommandSystem::handler_exit_conf_submodule,  "exit the configuration wifi menu"},
};

const commandList_t conf_server_commands[] = {
    {"get",        CommandSystem::handler_conf_server_get,      "print server configuration;"},
    {"set",        CommandSystem::handler_conf_server_set,      "set server configuration; usage: set ssid #SSID password #PASSWORD"},
    {"clear",      CommandSystem::handler_conf_server_clear,    "clear the stored server configuration"},
    {"exit",       CommandSystem::handler_exit_conf_submodule,  "exit the configuration server menu"},
};

const commandList_t conf_sensor_commands[] = {
    {"get",        CommandSystem::handler_conf_sensor_get,      "print sensor configuration;"},
    {"set",        CommandSystem::handler_conf_sensor_set,      "set server configuration; usage: set period #PERIOD"},
    {"clear",      CommandSystem::handler_conf_sensor_clear,    "clear the stored sensor configuration"},
    {"exit",       CommandSystem::handler_exit_conf_submodule,  "exit the configuration sensor menu"},
};

#endif  /* __COMMAND_SYSTEM_HPP__ */
