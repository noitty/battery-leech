#include "DatabaseClient.hpp"

DatabaseClient::DatabaseClient(void)
{ }

void DatabaseClient::connect(void)
{
    String url = String(conf.fields.influxdb_url);
    String org = String(conf.fields.influxdb_org);
    String bucket = String(conf.fields.influxdb_bucket);
    String token = String(conf.fields.influxdb_token);
    m_client.setConnectionParams(url.c_str(), org.c_str(), bucket.c_str(), token.c_str());
    m_client.setInsecure();
    delay(1000);    /* Wait 1 second to establish connection */
    m_connected = m_client.validateConnection();
}

void DatabaseClient::send_data(void)
{
    bool correct;
    /* Clean the values */
    String id = String(conf.fields.id);
    if(id.length() > 0) {
        Point measurement(id);
        measurement.clearFields();
        measurement.addField("Temperature", sensors->read_temperature(&correct) + conf.fields.calib_temp);
        measurement.addField("Humidity", sensors->read_humidity(&correct) + conf.fields.calib_hum);
        measurement.addField("Voltage", sensors->read_voltage(&correct) * conf.fields.calib_volt);
        m_client.writePoint(measurement);
    }
}
