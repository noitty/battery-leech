#ifndef __DATABASE_CLIENT_HPP__
#define __DATABASE_CLIENT_HPP__

#include <Arduino.h>
#include <InfluxDbClient.h>
#include "../configuration/Configuration.hpp"
#include "../sensors/SensorSystem.hpp"

extern Configuration conf;
extern SensorSystem *sensors;

class DatabaseClient
{
public:
    DatabaseClient(void);
    void connect(void);
    bool is_connected(void) {return m_connected; }
    void send_data(void);
    
private:
    bool m_connected;
    InfluxDBClient m_client;
};

#endif  /* __DATABASE_CLIENT_HPP__ */
