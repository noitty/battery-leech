#ifndef __SENSOR_SYSTEM_HPP__
#define __SENSOR_SYSTEM_HPP__

#include <Adafruit_AHTX0.h>

#define INIT_TRIES      3
#define INIT_DELAY      100 /* ms */
#define TH_STATUS_BIT   0x01 << 1
#define POW_PIN         A0
#define POW_VREF        3.3
#define POW_BITS        10

class SensorSystem
{
public:
    void init(void);
    bool init_th(void);
    float read_temperature(bool *correct);
    float read_humidity(bool *correct);
    float read_voltage(bool *correct);
    bool is_th_init(void) { return ((m_init_status & TH_STATUS_BIT) > 0); }
    bool is_init(void) { return is_th_init(); }
    void set_pow_led_up(void);
    void set_pow_led_down(void);

private:
    uint8_t m_init_status;
    Adafruit_AHTX0 m_aht_dev;
    sensors_event_t m_humidity; 
    sensors_event_t m_temperature;
};

#endif /* __SENSOR_SYSTEM_HPP__ */
