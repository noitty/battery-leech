#include "SensorSystem.hpp"

void SensorSystem::init(void) 
{
    /* Init the TH */
    uint8_t counter = 0;
    while(init_th() == false && counter < INIT_TRIES) {
        delay(INIT_DELAY); /* wait a while and check again */
        counter ++;
    }
    if(counter < INIT_TRIES) {
        /* Correctly initialized */
        m_init_status |= TH_STATUS_BIT;
        Serial.println("Initialized correctly TH");
    } else {
        Serial.println("Impossible to initialize TH");
    }
    delay(500);    /* provide time to print all the init log */
}

bool SensorSystem::init_th(void)
{
    return m_aht_dev.begin();
}

float SensorSystem::read_temperature(bool *correct)
{
    (*correct) = (m_aht_dev.getStatus() != 0xFF);
    if((*correct) == true) {
        m_aht_dev.getEvent(&m_humidity, &m_temperature);
    }
    return m_temperature.temperature;
}

float SensorSystem::read_humidity(bool *correct)
{
    (*correct) = (m_aht_dev.getStatus() != 0xFF);
    if((*correct) == true) {
        m_aht_dev.getEvent(&m_humidity, &m_temperature);
    }
    return m_humidity.relative_humidity;
}   

float SensorSystem::read_voltage(bool *correct)
{
    uint32_t measurement = analogRead(POW_PIN);
    /* ADC in arduino has 10-bits of resolution, and it is 
    * referred to 5V: value = read * (Vref / (2^n - 1))
    * where Vref = 5V; n = 10 bits
    */
   (*correct) = true;
    return float(measurement) * (POW_VREF / (std::pow(2, POW_BITS) - 1));
}   

void SensorSystem::set_pow_led_up(void)
{
    pinMode(LED_BUILTIN, OUTPUT); /* LED to show that is turned on */
    digitalWrite(LED_BUILTIN, LOW);
}

void SensorSystem::set_pow_led_down(void)
{
    pinMode(LED_BUILTIN, OUTPUT); /* LED to show that is turned off */
    digitalWrite(LED_BUILTIN, HIGH);
} 