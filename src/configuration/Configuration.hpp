#ifndef __CONFIGURATION_HPP__
#define __CONFIGURATION_HPP__

#define ITEM_SIZE       64
#define TOKEN_SIZE      128
#define CONF_LENGTH     ITEM_SIZE * 6 + TOKEN_SIZE + sizeof(uint32_t) + sizeof(float) * 3 + sizeof(uint8_t)

typedef union __attribute__ ((__packed__)) Configuration {
    uint8_t raw[CONF_LENGTH];
    struct __attribute__ ((__packed__)) {
        char wifi_ssid[ITEM_SIZE];         /* assumed maximum 24 characters of the SSID */
        char wifi_password[ITEM_SIZE];     /* assumed maximum 32 characters of password */
        char influxdb_url[ITEM_SIZE];
        char influxdb_token[TOKEN_SIZE];
        char influxdb_org[ITEM_SIZE];
        char influxdb_bucket[ITEM_SIZE];
        uint32_t measure_period;
        char id[ITEM_SIZE];
        float calib_temp;
        float calib_hum;
        float calib_volt;
        uint8_t sleep_mode;
    } fields;
} Configuration;

#endif  /* __CONFIGURATION_HPP__ */
