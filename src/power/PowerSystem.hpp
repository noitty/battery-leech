#ifndef __POWER_SYSTEM_HPP__
#define __POWER_SYSTEM_HPP__

#include <Arduino.h>
#include "../configuration/Configuration.hpp"
#include "../wifi/WifiSystem.hpp"
#include "../sensors/SensorSystem.hpp"
#include "../client/DatabaseClient.hpp"
extern "C" {
#include "user_interface.h"
}

#define ENABLED 1
#define WAKEUP_PIN  D0
#define MAX_TIME_LIGHT_SLEEP    10   /* in seconds; from https://docs.espressif.com/projects/esp8266-rtos-sdk/en/latest/api-reference/system/sleep_modes.html */

extern Configuration conf;
extern SensorSystem* sensors;
extern DatabaseClient client;

class PowerSystem
{
public:
    static void init(void);
    static void run_sleep(void (*callback)());
    static void run_modem_sleep(void);
    static void run_light_sleep(void (*callback)());
    static void run_deep_sleep(void);
};

#endif  /* __POWER_SYSTEM_HPP__ */