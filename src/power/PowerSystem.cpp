#include "PowerSystem.hpp"

void PowerSystem::init(void)
{
    pinMode(WAKEUP_PIN, INPUT_PULLUP);
    
}

void PowerSystem::run_modem_sleep(void)
{
    sensors->set_pow_led_down();
    WifiSystem::power_off();
    // Serial.println("Going to sleep ...");
    delay(conf.fields.measure_period * 10e3 + 1);   /* it must be executed to enter in sleeing mode of the WiFi */
    // Serial.println("Up!");
    sensors->set_pow_led_up();
    WifiSystem::power_on();
}

void PowerSystem::run_light_sleep(void (*callback)())
{
    gpio_pin_wakeup_enable(GPIO_ID_PIN(WAKEUP_PIN), GPIO_PIN_INTR_LOLEVEL);
    wifi_set_opmode(NULL_MODE);
    wifi_fpm_set_sleep_type(LIGHT_SLEEP_T);
    wifi_fpm_open();
    wifi_fpm_set_wakeup_cb(callback);
    sensors->set_pow_led_down();
    Serial.println("Going to sleep ...");
    Serial.flush();
    Serial.println(conf.fields.measure_period);
    Serial.flush();
    int iterations = (int)(conf.fields.measure_period / MAX_TIME_LIGHT_SLEEP);
    for(int i = 0; i < iterations; i++) {
        wifi_fpm_do_sleep(MAX_TIME_LIGHT_SLEEP * 10e6);
        delay(MAX_TIME_LIGHT_SLEEP * 10e3 + 1);
        if(conf.fields.sleep_mode != ENABLED) {
            break;
        }
    }
    if(conf.fields.sleep_mode == ENABLED) {
        int still_to_sleep = conf.fields.measure_period - MAX_TIME_LIGHT_SLEEP * iterations; 
        if(still_to_sleep > 0) {
            wifi_fpm_do_sleep(still_to_sleep * 10e6);
            delay(still_to_sleep * 10e3 + 1);
        }
    }
    Serial.println("Up!");
    Serial.flush();
    /* Init everything */
    sensors->set_pow_led_up();
    Serial.println("Wifi Init...");
    Serial.flush();
    WifiSystem::init();
    Serial.println("Wifi Up!");
    Serial.flush();
}

void PowerSystem::run_deep_sleep(void)
{

}

void PowerSystem::run_sleep(void (*callback)())
{
    if(conf.fields.sleep_mode == ENABLED) {
        /* select the type of sleep mode */
        run_modem_sleep();
        // run_light_sleep(callback);
    }
}