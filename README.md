# battery-leech

This project includes the source code of the module connected to the battery that monitors its status. It is part of the project "self-harvesting system". The

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

# Compile and Deploy

# Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

# Roadmap
The following planed developments in the "self-harvesting system" are listed as follows:
- [ ] Enabling an architecture with Low Power Modes
- [ ] Compact design of a single PCB with all the components
- [ ] Feeder system to connect devices 
- [ ] Modular solar panel system
- [ ] Extension of battery systems

# Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

# Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

# License
This project is licensed under the GNU General Public License v3.0

# Project status
The battery-leech is a personal project, and will be developed during the available time slots. Nevertheless, the project is alive and novel developments are planed. 
