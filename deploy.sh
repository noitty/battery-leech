#! /usr/bin/bash

# Compile
arduino-cli compile -b esp8266:esp8266:d1 battery-leech.ino

# Upload
arduino-cli upload -p /dev/ttyUSB0 -b esp8266:esp8266:d1 battery-leech.ino