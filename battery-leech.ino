
#include "user_interface.h"
#include "src/commands/CommandSystem.hpp"
#include "src/sensors/SensorSystem.hpp" 
#include "src/configuration/Configuration.hpp"
#include "src/wifi/WifiSystem.hpp"
#include "src/storage/StorageSystem.hpp"
#include "src/client/DatabaseClient.hpp"
#include "src/power/PowerSystem.hpp"

#define TZ_INFO "CET-1CEST,M3.5.0,M10.5.0/3"

CommandSystem cmd;
SensorSystem* sensors;
Configuration conf;
DatabaseClient client;
unsigned long next;

void wakeup_callback() {
  
  if(digitalRead(WAKEUP_PIN) == LOW) {
    conf.fields.sleep_mode = 0;
    StorageSystem::write_conf(conf);
  }
  Serial.print("All up!");
  Serial.flush();
}


void setup() {
  Serial.begin(9600);
  /* Read the conf */
  StorageSystem::read_conf(conf);
  /* Initialize the sensor system */
  sensors = new SensorSystem();
  sensors-> init();  
  sensors->set_pow_led_up();
  /* Init WiFi system */
  WifiSystem::init();  /* Only station, not access point */
  if(WifiSystem::is_connected() == true) {
    timeSync(TZ_INFO, "pool.ntp.org");
    Serial.println("Connecting to database server " + String(conf.fields.influxdb_url));
    client.connect();
    if(client.is_connected() == true) {
      Serial.println("Connected to " + String(conf.fields.influxdb_url));
    } else {
      Serial.println("Impossible to connect " + String(conf.fields.influxdb_url));
    }
  }
  /* Start the commanding system */
  cmd.begin(&Serial);
  next = millis();  /* force to measure at the beginning */
}

void loop() {
  cmd.run();
  if(WifiSystem::is_connected() && client.is_connected() == true && millis() >= next) {
    client.send_data();
    next = millis() + conf.fields.measure_period * 10e3;
    Serial.println("Next measurement at " + next);
  }
  // PowerSystem::run_sleep(wakeup_callback);
}